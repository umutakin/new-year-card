const card = document.querySelector(".card");

let cardClicked = false;

card.addEventListener("click", function () {
    card.classList.toggle("open");

    if (audio.paused && cardClicked == false) {
        icon.src = "x-music-on.png";
        cardClicked = true;
        audio.play();
    }
    // } else if (audio.playing) {
    //     icon.src = "music-off.png";
    //     audio.pause();
    // }
});

function generateRandomNumber(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function createSnowflake() {
    // Create the snowflake element
    var snowflake = document.createElement("div");
    snowflake.className = "snowflake";

    snowflake.innerHTML = "❄️";

    // Set the size of the snowflake using the random number generator
    var size = generateRandomNumber(10, 20);
    snowflake.style.width = size + "px";
    snowflake.style.height = size + "px";

    snowflake.style.fontSize = size + "px";

    // Set the position of the snowflake using the random number generator
    var left = generateRandomNumber(0, window.innerWidth - size);
    var top = generateRandomNumber(0, window.innerHeight - size);
    snowflake.style.left = left + "px";
    snowflake.style.top = top + "px";

    // Add the snowflake to the page
    const snowflakes = document.querySelector(".snowflakes");
    // document.body.appendChild(snowflake);
    snowflakes.appendChild(snowflake);
}

function addSnowflakes() {
    setInterval(function () {
        createSnowflake();
    }, 500); // call createSnowflake every 5 seconds
    // for (var i = 0; i < 10; i++) {
    //     createSnowflake();
    // }
}

const audio = document.getElementById("audio-file");

// function toggleAudio() {
//     // var audio = document.getElementById("audio-file");
//     if (audio.paused) {
//         audio.play();
//     } else {
//         audio.pause();
//     }
// }

const icon = document.getElementById("notes-icon");
icon.src = "x-music-off.png";
// const audio = new Audio("https://example.com/audio.mp3");

icon.addEventListener("click", () => {
    if (audio.paused) {
        icon.src = "x-music-on.png";
        audio.play();
    } else {
        icon.src = "x-music-off.png";
        audio.pause();
    }
});
